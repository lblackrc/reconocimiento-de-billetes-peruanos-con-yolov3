import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

"""
fileHandler = open('predicciones.txt', 'r')
listaLineas = fileHandler.readlines()
listaPredicciones = listaLineas[0].strip().split(' ')

clases = {}
for i in listaPredicciones:
    clases[i] = 0

for i in listaPredicciones:
    clases[i]+=1

print(clases)

print(len(listaPredicciones))
fileHandler.close()

"""

def plot_confusion_matrix(df_confusion, title='Confusion matrix', cmap=plt.cm.gray_r):
    plt.matshow(df_confusion, cmap=cmap) # imshow
    #plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(df_confusion.columns))
    plt.xticks(tick_marks, df_confusion.columns, rotation=45)
    plt.yticks(tick_marks, df_confusion.index)
    #plt.tight_layout()
    plt.ylabel(df_confusion.index.name)
    plt.xlabel(df_confusion.columns.name)



fileHandler = open('labelsReales.txt', 'r')
listaLineas = fileHandler.readlines()
listaLabelsReales = listaLineas[0].strip().split(' ')
fileHandler.close()

fileHandler = open('predicciones.txt', 'r')
listaLineas = fileHandler.readlines()
listaPredicciones = listaLineas[0].strip().split(' ')
fileHandler.close()

#MATRIZ DE CONFUSIÓN
print("Predicción con etiquetas con las imagenes  :  ")
y_actu=list()
y_pred=list()

y_actu = listaLabelsReales
y_pred = listaPredicciones

y_actu = pd.Series(y_actu, name='Actual')
y_pred = pd.Series(y_pred, name='Predicted')


df_confusion = pd.crosstab(y_actu, y_pred,margins=True)
print(df_confusion)

plot_confusion_matrix(df_confusion)

df = pd.DataFrame(0,index='0 1 2 3 4 5 6 7 8'.split(),columns='TP_Rate FP_Rate'.split())